set export
ssh-key := "hcloud-kubernetes"
cluster-manifest := "demo-cluster.yaml"
workload-kubeconfig := "my-kubeconfig"

_default:
	@just --list

# create management cluster using kind
management-cluster:
	kind create cluster --image=kindest/node:v1.29.0

# install CAPH provider
get-caph:
	clusterctl init --infrastructure hetzner

# creates ssh key to be used by caph
[confirm("this recipe will create ssh key in your current working directory. Do you want that? [y/n]")]
create-ssh-key: clean-ssh-key
	ssh-keygen -f {{ssh-key}} -N "" -C "hcloud-kubernetes"

# clean generated ssh key
clean-ssh-key:
	#!/usr/bin/env bash
	if [[ -f {{ssh-key}} && -f {{ssh-key}}.pub ]]; then
		rm {{ssh-key}} {{ssh-key}}.pub
	fi

# add content of ssh-key to hetzner cloud
add-ssh-pub-key:
	hcloud ssh-key create --public-key-from-file=hcloud-kubernetes.pub --name=hcloud-kubernetes

# create secret so that controller can talk to hetzner
create-hcloud-kubernetes-secret:
	kubectl create secret generic hetzner --from-literal=hcloud=$HCLOUD_TOKEN

# create manifests for cluster and other resources
create-templates:
	#!/usr/bin/env bash
	set -euo pipefail
	if [[ -f {{cluster-manifest}} ]]; then
		echo "cluster manifest is already present, applying..."
	else
		clusterctl generate cluster demo-cluster --kubernetes-version 1.27.10 --worker-machine-count=1 > {{cluster-manifest}}
	fi

# just create the cluster
cluster: create-hcloud-kubernetes-secret create-templates && deploy-cilium
	kubectl apply -f {{cluster-manifest}}

# after cluster is created 

# get kubeconfig of the workload cluster 
get-kubeconfig:
	# kubectl wait --for=condition=ready --timeout=300s kcp --all
	clusterctl get kubeconfig demo-cluster > {{workload-kubeconfig}}

# install cilium as CNI in the cluster
deploy-cilium: get-kubeconfig
	helm repo add cilium https://helm.cilium.io/
	helm repo update
	helm install cilium cilium/cilium --version 1.15.1 --namespace kube-system --kubeconfig {{workload-kubeconfig}}

# open up k9s in workload cluster
k9s-workload-cluster:
	k9s --kubeconfig {{workload-kubeconfig}}
